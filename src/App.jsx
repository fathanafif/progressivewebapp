import React, { useState } from "react";
import { fetchWeather } from "./api/fetchWeather";
import { CSSTransition } from "react-transition-group";
import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import * as Svg from "./svg.jsx";

function App() {
  const [query, setQuery] = useState("");
  const [weather, setWeather] = useState(true);
  // const [showResult, setShowResult]= useState(true);

  const search = async (e) => {
    if (e.key === "Enter") {
      const data = await fetchWeather(query);
      console.log(data);
      setWeather(false);
      setWeather(data);
      setQuery("");
    }
  };

  const handleOnClick = async (e) => {
    e.preventDefault();
    const data = await fetchWeather(query);
    setWeather(false);
    setWeather(data);
    setQuery("");
  };

  return (
    <div className="container-fullwidth">
      <div className="header">
        Weather
        <span className="vertical-three-dots">
          <Svg.ThreeDots/>
        </span>
      </div>

      <div className="search-container">
        <div className="input-group">
          <input type="text" className="form-control search" placeholder="Search city.." value={query} onChange={(e) => setQuery(e.target.value)} onKeyPress={search}/>
          <div className="input-group-append" onClick={handleOnClick}>
            <span className="btn input-group-text search-append">
              <span className="search-icon">
                <Svg.Search/>
              </span>
            </span>
          </div>
        </div>
      </div>

      {weather.main && (
        <div className="result-container">
          <CSSTransition in={true} appear={true} timeout={1000} classNames="fade-first">
          <div className="card mb-4">
              <div className="first-section row">
                <h5>
                  {weather.name} - {weather.sys.country}
                </h5>
              </div>
              <div className="second-section row font-weight-normal">
                {Math.round(weather.main.temp - 273.15)}
                <sup className="degree">&deg;c</sup>
              </div>
              <div className="third-section">
                <img width="144px" height="144px" src={`http://openweathermap.org/img/wn/` + weather.weather[0].icon + `@2x.png`} alt=""/>
                <p className="m-0">{weather.weather[0].description}</p>
              </div>
            </div>
          </CSSTransition>
          <CSSTransition in={true} appear={true} timeout={1600} classNames="fade-second">
          <div className="container col-12">
            <div className="row">
              <div className="card col p-3 mr-2">
                <div>Wind</div>
                <div className="p-3 sm">
                  Speed
                  <br/>
                  <h4 className="my-0">
                    {weather.wind.speed}
                    <sub className="sm">kn</sub>
                  </h4>
                  <div className="pt-2 sm">
                    Degree
                    <br/>
                    <h4 className="my-0">
                      {weather.wind.deg}
                      <sup>&deg;</sup>
                    </h4>
                  </div>
                </div>
              </div>
              <div className="card col p-3 ml-2">
                <div>Polution</div>
                <div className="p-3 sm">
                  Cloud
                  <br/>
                  <h4 className="my-0">{weather.clouds.all}</h4>
                  <div className="pt-2 sm">
                    Visibility
                    <br/>
                    <h4 className="my-0">{weather.visibility}</h4>
                  </div>
                </div>
              </div>
            </div>
          </div>
          </CSSTransition>
          </div>
      )}

      <div className="navbar fix-bottom text-light justify-content-center">
        <div className="row col text-center">
          <div className="col-3 nav-icon">
            <Svg.House/>
          </div>
          <div className="col-3 nav-icon">
            <Svg.BubbleChat/>
          </div>
          <div className="col-3 nav-icon">
            <Svg.Tag/>
          </div>
          <div className="col-3 nav-icon">
            <Svg.Account/>
          </div>
        </div>
      </div>
    </div>
  );
}

export default App;
