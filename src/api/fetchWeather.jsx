import Axios from 'axios';

const URL = 'https://api.openweathermap.org/data/2.5/weather';
const API_KEY = '9a3211ad6d3e10a81550cec710a720d1';

export const fetchWeather = async (query) => {
    const {data} = await Axios.get(URL, {
        params: {
            q: query,
            unites: 'metric',
            APPID: API_KEY
        }
    });
    return data;
}
